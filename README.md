# First_api_go



## Getting started

### Démarer les containers

```
docker compose up --build
```

### Créer les migrations

```bash
migrate create -ext sql -dir migrations -seq create_albums_table
```

 apply a migration
 ```bash
migrate -database postgresql://test:test@db:5432/api_go?sslmode=disable  -path migrations up
 ```

