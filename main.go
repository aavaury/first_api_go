package main

import (
	"github.com/gin-gonic/gin"
	"api_go/db"
	"api_go/routes"
	"log"

	_ "github.com/lib/pq"
)

func main() {

	// Initialize the database
	if err := db.InitDB(); err != nil {
		log.Fatalf("Failed to initialize the database: %v", err)
	}

	// Create a new Gin router
	router := gin.Default()

	// Define routes
	router.GET("/", routes.Welcome)
	router.GET("/albums", routes.GetAlbums)

	// Run the server
	router.Run("0.0.0.0:5555")
}