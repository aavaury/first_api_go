# Première étape : builder
FROM golang:bookworm AS builder

# Installation des outils spécifiques à Go
RUN \
    # Serveur de langage Go : utilisé par vscode pour travailler avec Go
    go install golang.org/x/tools/gopls@latest \
    # Importation automatique et formatage Go
    && go install golang.org/x/tools/cmd/goimports@latest \
    # Linter Go
    && go install honnef.co/go/tools/cmd/staticcheck@latest \
    # Débogueur Go
    && go install github.com/go-delve/delve/cmd/dlv@latest \
    # Rechargement automatique Go
    && go install github.com/cosmtrek/air@latest \
    # Outil de migration Postgres
    && go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

# Copie de go.mod et go.sum
COPY go.mod .
COPY go.sum .

# Téléchargement des dépendances
RUN go mod download

# Copie du reste des fichiers
COPY . .

# Construction de l'application
RUN go build -o main .

# Deuxième étape : exécution
FROM builder AS execution

RUN go build .

# Spécification de l'étape de démarrage
CMD ["./main"]
