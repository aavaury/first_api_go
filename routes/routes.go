// routes/routes.go
package routes

import (
	"net/http"
	"fmt"

	"github.com/gin-gonic/gin"
	"api_go/db"
)

// Fonction pour gérer la route de bienvenue
func Welcome(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, "Bienvenue !")
}

func GetAlbums(c *gin.Context) {
	albums, err := db.GetAlbumsFromDB()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("Failed to fetch albums from the database: %v", err)})
		return
	}

	c.JSON(http.StatusOK, albums)
}