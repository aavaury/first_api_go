package db

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
	"api_go/models"
)

var DB *sql.DB

func InitDB() error {
	retries := 5
	for i := 1; i <= retries; i++ {
		db, err := sql.Open("postgres", "postgres://test:test@db/api_go?sslmode=disable")

		if err != nil {
			fmt.Printf("Failed to connect to the database (attempt %d/%d): %v\n", i, retries, err)
			time.Sleep(5 * time.Second) // Add a delay before retrying
			continue
		}

		err = db.Ping()
		if err != nil {
			fmt.Printf("Failed to ping the database (attempt %d/%d): %v\n", i, retries, err)
			db.Close()
			time.Sleep(5 * time.Second) // Add a delay before retrying
			continue
		}

		DB = db

		fmt.Println("Connected to the database")
		return nil
	}

	return fmt.Errorf("unable to connect to the database after %d attempts", retries)
}


func GetAlbumsFromDB() ([]models.Album, error) {
	// Exemple de requête SQL pour récupérer tous les albums
	query := "SELECT id, title, artist FROM albums"

	// Exécuter la requête SQL
	rows, err := DB.Query(query)
	if err != nil {
		return nil, fmt.Errorf("error executing query: %v", err)
	}
	defer rows.Close()

	fmt.Println("Query executed successfully")

	// Parcourir les résultats et construire la liste d'albums
	var albums []models.Album
	for rows.Next() {
		var album models.Album
		err := rows.Scan(&album.ID, &album.Title, &album.Artist)
		if err != nil {
			return nil, fmt.Errorf("error scanning row: %v", err)
		}
		albums = append(albums, album)
	}

	fmt.Println("Scanning rows completed successfully")

	// Vérifier s'il y a des erreurs lors du parcours des résultats
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error iterating over results: %v", err)
	}

	fmt.Println("Returning albums:", albums)

	// Vérifier si aucun album n'a été récupéré
	if len(albums) == 0 {
		return nil, fmt.Errorf("no albums found in the database")
	}

	return albums, nil
}
