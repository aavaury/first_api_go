-- Création de la base de données
CREATE DATABASE IF NOT EXISTS test;

-- Utilisation de la base de données
USE test;

-- Début de la transaction
START TRANSACTION;

-- Création de la table
CREATE TABLE IF NOT EXISTS albums (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    artist VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL
);

-- Insertion des données
INSERT INTO albums (title, artist, price) VALUES
    ('Album 1', 'Artist 1', 19.99),
    ('Album 2', 'Artist 2', 29.99),
    ('Album 3', 'Artist 3', 14.99);

-- Fin de la transaction
COMMIT;
